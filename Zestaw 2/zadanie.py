import random as r
import math as m
import pylab

allPi = []
def symulujRzut(ileRazy):
    wpadlo = 0
    for i in range(0, ileRazy):
        # generuje liczby z zakresu (0,1)
        x2 = r.random()**2
        y2 = r.random()**2
        if (m.sqrt(x2 + y2) < 1.0): #nierownosc
            wpadlo += 1
    pi = (float(wpadlo) / ileRazy) * 4
    # wpadlo / wszystkieRzuty = pi / 4
    print(pi)
    allPi.append(pi)


symulujRzut(10)
symulujRzut(pow(10,2))
symulujRzut(pow(10,3))
symulujRzut(pow(10,5))
symulujRzut(pow(10,6))
symulujRzut(pow(10,7))

pylab.plot(allPi)
pylab.grid(True)
pylab.xlabel("Ile prób")
pylab.ylabel("Wartość pi")
pylab.show()