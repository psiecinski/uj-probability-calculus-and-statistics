import matplotlib.pyplot as plt
import numpy
import random
import math

sections = 100
numbers = 100000
x1, y1, x2, y2 = [], [], [], []

value = [0 for x in range(sections)]
for val in numpy.arange(0, 1-(1/sections), (1/sections)):
    x1.append(val)
    y1.append(val/(math.sqrt(1-(pow(val,2)))))

for i in range(numbers):
    rand = random.random()
    x = math.sin(math.acos((-2)*rand+1)) 
    value[int(x*sections)] += 1

for val in numpy.arange(0, 1-(1/sections), (1/sections)):
    x2.append(val)
    y2.append(((value[math.floor(val*sections)]/numbers)/(1/sections)))

plt.plot(x1, y1, label="policzone")
plt.plot(x2, y2, label="generowane liczby")
plt.title(label="Zestaw 5 zadanie 2e", 
          fontsize=20) 
plt.legend()
plt.show()



