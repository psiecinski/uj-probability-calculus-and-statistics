import math
import random
import numpy
import matplotlib.pyplot as plt
import seaborn as sns

parameter = 5
wartosci = []

def Poisson(lambd):
    q = math.exp(-lambd)
    k = 0
    s = q
    p = q
    u = numpy.random.uniform(0, 1)
    while u > s:
        k = k+1
        p = (p * lambd)/k
        s = s + p
    return k

for i in range(0,10000):
    wartosci.append(Poisson(parameter))

wartosci2 = wartosci
wartosci2.sort()
rep = {i:wartosci2.count(i) for i in wartosci2}
print(rep)

oczekiwana = numpy.random.poisson(lam=parameter, size=10000)

arr = plt.hist(wartosci)
plt.show()
arr = plt.hist(wartosci, bins=14, density=True)

sns.distplot(numpy.random.poisson(lam=5, size=100), hist=False, label='poisson')
plt.suptitle('Przy pomocy algorytmu z wykladu', fontsize=14, fontweight='bold')
plt.show()
