import random 
import math
import matplotlib.pyplot as plt

def generate(n):
    x = 0
    y = 0
    fx = 0
    bx = []
    yx = []
    xr = [-1, 0, 1, 2, 3]
    yr = [0, 1000, 1000, 1000, 0]

    for i in range(0,n):
        x = (random.uniform(0, 1) * 4) -1
        y = random.uniform(0, 1) * (1/3)

        if -1 <= x <= 0:
            fx = (1/3 * x) + 1/3 
        elif 0 <= x <= 2:
            fx = 1/3
        elif 2 <= x <= 3:
            fx = (-1/3 * x) + 1

        if y <= fx:
            bx.append(x)
            yx.append(y)
        
    plt.hist(bx, bins=100)
    plt.plot(xr,yr)
    plt.suptitle('Histogram - funkcja gestosci prawdopodobienstwa dla '+str(n)+' prob')
    plt.show()



# print(generate(100))
# print(generate(1000))
print(generate(100000))