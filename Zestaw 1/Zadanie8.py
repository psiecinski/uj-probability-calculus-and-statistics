# Pawel Siecinski - Zadanie 8
import random
import pylab
import numpy as np
randseed = random.uniform(0, 1)
random.seed(randseed)
trefl = 0
udanaProba = 0

# Symulacja wyciagniecia jednej karty trzykrotnie ===== 

def wyciagnijKarte(karty, liczbaLosowa):
    wylosowanaKarta = karty[liczbaLosowa]
    del karty[liczbaLosowa]
    return wylosowanaKarta

def eksperyment(nrProby):
    global trefl
    karty = ["trefl","trefl","trefl","trefl","trefl","trefl","trefl","trefl","trefl","trefl","trefl","trefl","trefl",
        "pik","pik","pik","pik","pik","pik","pik","pik","pik","pik","pik","pik","pik",
        "kier","kier","kier","kier","kier","kier","kier","kier","kier","kier","kier","kier","kier",
        "karo","karo","karo","karo","karo","karo","karo","karo","karo","karo","karo","karo","karo",]
    jestTrefl = False

    for i in range(0,3): # 3 razy
        liczbaLosowa = random.randint(0, len(karty)-1)
        karta = wyciagnijKarte(karty, liczbaLosowa)
        if(karta == "trefl"): 
            jestTrefl = True
            trefl+=1
    #podpunkt b
    # if jestTrefl :
    #     print("Proba {}: wsrod kart znajduje sie trefl".format(nrProby))
    # else:
    #     print("Proba {}: wsrod kart NIE znajduje sie trefl".format(nrProby))
    return jestTrefl

liczbaWszystkichEksperymentow = 0
wynik = 0
udanyEksperyment = 0
arrayofPoints = []
while(round(wynik,3)!=0.413):
    liczbaWszystkichEksperymentow+=1
    czyJestTrefl = eksperyment(liczbaWszystkichEksperymentow)
    if(czyJestTrefl == False): 
        udanyEksperyment+=1 
    wynik = udanyEksperyment/liczbaWszystkichEksperymentow
    arrayofPoints.append(wynik) ## do matplotliba
    
print("Ile potrzeba bylo prob aby dokladnosc byla 0.413: ", liczbaWszystkichEksperymentow)

# =================
#   Wyswietlanie
# =================
horiz_line_data = np.array([0.413 for i in range(len(arrayofPoints))])
pylab.plot(horiz_line_data)
pylab.plot(arrayofPoints)
pylab.grid(True)
pylab.show()